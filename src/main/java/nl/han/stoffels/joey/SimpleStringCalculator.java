package nl.han.stoffels.joey;

public class SimpleStringCalculator {

    protected int add(String ... numbers) {

        int totalValue = 0;

        for (String number : numbers) {
           if (number.equals("")) break;
            totalValue += Integer.parseInt(number);
        }

        return totalValue;
    }

}

