package nl.han.stoffels.joey;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleStringCalculatorTest {

    private SimpleStringCalculator simpleStringCalculator;

    @Before
    public void init() {
        this.simpleStringCalculator = new SimpleStringCalculator();
    }

    @Test
    public void testAdd_EmptyString() {
        String testString = "";
        int expectedTotalValue = 0;
        testAdd(expectedTotalValue, testString);
    }

    @Test
    public void testAdd_SingleValue() {
        String testString = "1";
        int expectedTotalValue = 1;
        testAdd(expectedTotalValue, testString);
    }

    @Test
    public void testAdd_MultipleValues() {
        String[] testString = {"1", "2", "5"};
        int expectedTotalValue = 8;
        testAdd(expectedTotalValue, testString);
    }

    @Test
    public void testAddMultipleStrings() {
        String[] testString = {"1", "2", "5"};
        int expectedTotalValue = 8;
        testAdd(expectedTotalValue, testString);
    }

    private void testAdd(int expectedTotalValue, String ... numbers) {
        Assert.assertEquals(expectedTotalValue, simpleStringCalculator.add(numbers));
    }

}
